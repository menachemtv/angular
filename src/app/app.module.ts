import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { PostsService } from './posts/posts.service';
import { UsersService } from './users/users.service';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';

 export const firebaseConfig = {
    apiKey: "AIzaSyCltZoazlcfTyEe4tVSRk0r8FSINAiyPkk",
    authDomain: "angularclass-346d3.firebaseapp.com",
    databaseURL: "https://angularclass-346d3.firebaseio.com",
    storageBucket: "angularclass-346d3.appspot.com",
    messagingSenderId: "849525226262"
 }

const appRoutes:Routes = [ //building routes according to url entered.
  {path:'users',component:UsersComponent},//when relize path is users load user component.
  {path:'posts',component:PostsComponent},//when relize path is posts load post component.
  {path:'',component:PostsComponent},// when no path inserted load Posts component as default. 
  {path:'**',component:PageNotFoundComponent}//when path doesn't exist, load error component.
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserComponent,
    UserFormComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
