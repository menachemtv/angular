import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
     .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }
  `]
})
export class UsersComponent implements OnInit {

  users;
  currentUser;
 

  select(user){
    this.currentUser = user;
  }
    updateUser(user){
    this._usersService.updateuser(user);
   }
    adduser(user){
     this._usersService.adduser(user);//add user through service file where there is an add user function.
}
editUser(user){
    this._usersService.updateuser(user);
 }
   deleteUser(user){
    this._usersService.deleteuser(user);//add user through service file where there is an add user function.       
  }
  constructor(private _usersService:UsersService) { }
 
   ngOnInit() {
this._usersService.getUsers().subscribe(usersData => this.users = usersData);
   }
 
 }