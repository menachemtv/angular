import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
    styles: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }
  `]
})
export class PostsComponent implements OnInit {
isLoading: Boolean=true;
 
  posts;
  currentPost;
  
  select(post){
    this.currentPost = post;
  }
      addpost(post){
     this._postsService.addpost(post);//add user through service file where there is an add user function.
}
editPost(post){
    this._postsService.updatepost(post);
 }
 deletePost(post){
    this._postsService.deletepost(post);//add user through service file where there is an add user function.       
    
 }
  constructor(private _postsService: PostsService) { }

  ngOnInit() {
    this._postsService.getPosts().subscribe(postsData => 
    {this.posts = postsData;
       this.isLoading =false});
  }

}
